import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators, FormArray } from "@angular/forms";
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { ApiCallService } from 'src/app/services/api-call.service';
import { CommonService } from 'src/app/services/common.service';
import { mobileValidation, EmailValidation, panValidation, gstinValidation, ZipValidation } from '../../services/validations';

@Component({
  selector: 'app-createclient',
  templateUrl: './createclient.component.html',
  styleUrls: ['./createclient.component.scss']
})
export class CreateclientComponent implements OnInit {
  createClient: FormGroup;
  submitted: any;
  CompanyList: any;
  PlantList: any;
  CountryList: any;
  StateList: any;
  isActiveTab: number = 0;
  updateRow: any;
  BusinessPartnerList: any;
  displayedColumns = ['action', 'bpCode', 'bpFName', 'actGroupCode', 'shortKey', 'pan', 'gstNumber', 'street', 'city', 'postalCode', 'phone', 'email'];

  dataSource!: MatTableDataSource<BPClientData>;
  // viewDataSource!: MatTableDataSource<PlantData>;
  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;
  constructor(private fb: FormBuilder, private apiCallService: ApiCallService, private commonService: CommonService) {
    this.createClient = this.fb.group({
      bpCode: [null, [Validators.required]],
      cmpCode: [null, [Validators.required]],
      plantCode: [null, [Validators.required]],
      actGroupCode: ["CL", [Validators.required]],
      bpFName: [null, [Validators.required]],
      bpMName: [null],
      bpLName: [null],
      shortKey: [null, [Validators.required]],
      pan: [null, [Validators.required]],
      gstNumber: [null, [Validators.required]],
      addressLine1: [null],
      addressLine2: [null],
      street: [null, [Validators.required]],
      city: [null, [Validators.required]],
      postalCode: [null, [Validators.required, ZipValidation]],
      email: [null, [Validators.required, EmailValidation]],
      phone: [null, [Validators.required, mobileValidation]],
      stateCode: [null, [Validators.required]],
      countryCode: [null, [Validators.required]],
      status: ["1", [Validators.required]],
      createBy: [null],
      createOn: [null],
      updateBy: [null],
      updateOn: [null],
      isDeleted: [null],
      relationPersons: new FormArray([this.addContactPerson()]),
    })
  }
  ngOnInit(): void {
    document.body.style.backgroundColor = "#E5E7E9";
    this.getCompany();
    this.getCountry();
    this.getClient();
  }
  //Form Group Error
  public myError = (controlName: string, errorName: string) => {
    return this.createClient.controls[controlName].hasError(errorName);
  }
  //Form Array error 
  public myErrorArray = (controlName: string, errorName: string, i: any) => {
    return this.relationPersonsArr['controls'][i]['controls'][controlName].hasError(errorName)
  }
  // Active Deactive matTab change event for Add method or update method
  tabIsActive(value: any, change: any) {
    if (value != change) { this.updateRow = null, value = 0 }
    this.isActiveTab = value;
    // this.isCreate = value;
  }


  // Get relationPersonsArr Form Array
  get relationPersonsArr(): any {
    return <FormArray>this.createClient.get('relationPersons') as FormArray;
  }
  //Form Array
  private addContactPerson(): FormGroup {
    return this.fb.group({
      cmpCode: [null],
      plantCode: [null],
      persontype: [null, [Validators.required]],
      name: [null, [Validators.required]],
      actGroupCode: ["CP", [Validators.required]],
      phone: [null, [Validators.required, mobileValidation]],
      emailId: [null, [Validators.required, EmailValidation]],
      status: ["1", [Validators.required]],
      createBy: [null],
      createOn: [null],
      updateBy: [null],
      updateOn: [null],
      isDeleted: [null],
    })
  }

  //Delete Form array
  deletearr(i: any) {
    let control = <FormArray>this.createClient.controls.relationPersons;
    control.removeAt(i)
  }

  // ADD Click event of addressArray Form Array
  addPerson() {
    let control = <FormArray>this.createClient.controls.relationPersons;
    control.push(
      this.fb.group({
        cmpCode: [null],
        plantCode: [null],
        persontype: [null, [Validators.required]],
        name: [null, [Validators.required]],
        actGroupCode: ["CP", [Validators.required]],
        phone: [null, [Validators.required, mobileValidation]],
        emailId: [null, [Validators.required, EmailValidation]],
        status: ["1", [Validators.required]],
        createBy: [null],
        createOn: [null],
        updateBy: [null],
        updateOn: [null],
        isDeleted: [null],
      })
    );
  }

  // company view table filter
  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // Datasource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }

  getCompany() {
    var data = {}
    this.apiCallService.getCompany(data).subscribe({
      next: (result: any) => {
        if (result) {
          this.CompanyList = result;
        }
      }, error: (error: any) => {
        this.commonService.showError(error.error);
      }
    })
  }
  getPlant(cmpCode: any) {
    if (cmpCode) {
      var data = { cmpCode: cmpCode }
      this.apiCallService.getPlantId(data).subscribe({
        next: (result: any) => {
          if (result) {
            this.PlantList = result;
          }
        }, error: (error: any) => {
          this.commonService.showError(error.error);
        }
      })
    }
  }
  getClient() {
    var param={Cid:"1000",Pid:"1010",GP:"CL"}
    var data = {param:param}
    this.apiCallService.getBusinessPartnerByCompPlant(data).subscribe({
      next: (result: any) => {
        if (result) {
          this.BusinessPartnerList = result;

          // Assign the data to the data source for the table to render
          this.dataSource = new MatTableDataSource(this.BusinessPartnerList);
          this.dataSource.paginator = this.paginator;
          this.dataSource.sort = this.sort;
        }
      }, error: (error: any) => {
        this.commonService.showError(error.error);
      }
    })
  }
  getCountry() {
    var data = { param: null }
    this.apiCallService.getCountry(data).subscribe({
      next: (result: any) => {
        if (result) {
          this.CountryList = result;
        }
      }, error: (error: any) => {
        this.commonService.showError(error.error);
      }
    })
  }
  getState(CountryCode: any) {
    if (CountryCode) {
      var param = { CountryCode: CountryCode }
      var data = { param: param }
      this.apiCallService.getState(data).subscribe({
        next: (result: any) => {
          if (result) {
            this.StateList = result;
          }
        }, error: (error: any) => {
          this.commonService.showError(error.error);
        }
      })
    }
  }

  addClient() {
    if (this.createClient.invalid) {
      this.submitted = true;
      return;
    }
    var JSON = this.createClient.value;
    JSON.createBy = "100"; // Loged IN User ID
    JSON.updateBy = "100"; // Loged IN User ID

    console.log(JSON)
    JSON.relationPersons.find((item: any) => { item.cmpCode = JSON.cmpCode, item.CreateBy = JSON.createBy, item.UpdateBy = JSON.updateBy })
    var data = { data: JSON }
    console.log(data)
    this.apiCallService.addBusinessPartner(data).subscribe({
      next: (result: any) => {
        if (result) {
          this.commonService.showSuccess("Record Inserted Succefully!");
          this.createClient.reset();
        }
      }, error: (error: any) => {
        if (error.status == 200) {
          this.commonService.showSuccess(error.error.text);
          this.createClient.reset();
        } else {
          this.commonService.showError(error.error);
        }
      },

    })

  }


}




export interface BPClientData {
  bpCode: string,
  cmpCode: string,
  plantCode: string,
  actGroupCode: string,
  bpFName: string,
  bpMName: string,
  bpLName: string,
  shortKey: string,
  pan: string,
  gstNumber: string,
  addressLine1: string,
  addressLine2: string,
  street: string,
  city: string,
  postalCode: string,
  stateCode: string,
  countryCode: string,
  phone: string,
  email: string,
  status: string,
  createBy: string,
  createOn: string,
  updateBy: string,
  updateOn: string,
  isDeleted: string,
  relationPersons: string,
}