import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CreateclientRoutingModule } from './createclient-routing.module';
@NgModule({
  declarations: [],
  imports: [
    CommonModule, FormsModule, ReactiveFormsModule,
    CreateclientRoutingModule,
  ]
})
export class CreateclientModule { }
