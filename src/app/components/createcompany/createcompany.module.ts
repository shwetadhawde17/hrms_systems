import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CreatecompanyRoutingModule } from './createcompany-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
@NgModule({
  declarations: [],
  imports: [
    CommonModule, FormsModule, ReactiveFormsModule,
    CreatecompanyRoutingModule,
      ]
})
export class CreatecompanyModule { }
