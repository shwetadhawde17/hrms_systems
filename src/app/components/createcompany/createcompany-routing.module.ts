import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CreatecompanyComponent } from './createcompany.component';

const routes: Routes = [
  { path: '', component: CreatecompanyComponent, children: [] }
];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CreatecompanyRoutingModule { }
