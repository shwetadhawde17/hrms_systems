import { Component, OnInit, TemplateRef, ViewChild, ViewEncapsulation } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators, FormArray } from "@angular/forms";
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { ApiCallService } from 'src/app/services/api-call.service';
import { CommonService } from 'src/app/services/common.service';
import { mobileValidation, EmailValidation, panValidation, gstinValidation, ZipValidation, CINValidation } from '../../services/validations';

@Component({
  selector: 'app-createcompany',
  templateUrl: './createcompany.component.html',
  styleUrls: ['./createcompany.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class CreatecompanyComponent implements OnInit {
  panelOpenState: boolean = false;
  createCompany: FormGroup;
  submitted: any;
  CountryList: any;
  StateList: any;
  CompanyList: any;
  selectedFiles?: FileList;
  selectedFileNames: any;
  // View dataTabale
  displayedColumns = ['action', 'cmpCode', 'cmpName', 'cmpPAN', 'cmpCIN', 'email', 'street', 'city', 'postalCode'];
  displayedColumn = ['cmpCode', 'plantCode', 'plantName', 'shortKey', 'street', 'addressLine1', 'addressLine2', 'city', 'postalCode', 'countryCode', 'stateCode', 'email', 'phone'];

  dataSource!: MatTableDataSource<CmpData>;
  viewDataSource!: MatTableDataSource<PlantData>;
  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;
  updateRow: any;
  deleteRow: any;
  isActiveTab: number = 0;
  isCreate: number = 0;
  previews: any;
  constructor(private dialog: MatDialog, private fb: FormBuilder, private apiCallService: ApiCallService, private commonService: CommonService) {
    this.createCompany = this.fb.group({
      cmpCode: [null, [Validators.required]],
      cmpName: [null, [Validators.required]],
      shortKey: [null, [Validators.required]],
      cmpPAN: [null, [Validators.required, panValidation]],
      cin: [null, [Validators.required, CINValidation]],
      countryCode: [null, [Validators.required]],
      stateCode: [null, [Validators.required]],
      city: [null, [Validators.required]],
      street: [null, [Validators.required]],
      addressLine1: [null],
      addressLine2: [null],
      postalCode: [null, [Validators.required, ZipValidation]],
      email: [null, [Validators.required, EmailValidation]],
      phone: [null, [Validators.required, mobileValidation]],
      cmpLogo: [null],
      cmpWebSiteURL: [null, [Validators.required]],
      plants: new FormArray([this.addGSTNArrGroup()]),

      students: new FormArray([this.addstudentsGroup()]),
      createBy: [null],
      createOn: [null],
      updateBy: [null],
      updateOn: [null],
    })

  }
  ngOnInit(): void {
    document.body.style.backgroundColor = "#E5E7E9";
   
    this.getCountry();
    this.getCompany();
  }
  public myError = (controlName: string, errorName: string) => {
    return this.createCompany.controls[controlName].hasError(errorName);
  }
  public myErrorArray = (controlName: string, errorName: string, i: any) => {
    return this.plantsArray['controls'][i]['controls'][controlName].hasError(errorName)
  }

  // Mat Dialogs.
  @ViewChild('viewDialog') viewDialog!: TemplateRef<any>;
  @ViewChild('deleteDialog') deleteDialog!: TemplateRef<any>;
  openDialogWithTemplateRef(templateRef: TemplateRef<any>) {
    this.dialog.open(templateRef);
  }


  // Get plantsArray Form Array
  get plantsArray(): any {
    return <FormArray>this.createCompany.get('plants') as FormArray;
  }

  get studsArray(): any{
    return <FormArray>this.createCompany.get('students') as FormArray;
  }
  //Form Array
  private addGSTNArrGroup(): FormGroup {
    return this.fb.group({
      cmpCode: [null],
      plantCode: [null, [Validators.required]],
      plantName: [null, [Validators.required]],
      shortKey: [null, [Validators.required]],
      gstNumber: [null, [Validators.required, gstinValidation]],
      addressLine1: [null],
      addressLine2: [null],
      street: [null, [Validators.required]],
      city: [null, [Validators.required]],
      postalCode: [null, [Validators.required]],
      stateCode: [null, [Validators.required]],
      countryCode: [null, [Validators.required]],
      email: [null, [Validators.required, EmailValidation]],
      phone: [null, [Validators.required, mobileValidation]],
      createBy: [null],
      createOn: [null],
      updateBy: [null],
      updateOn: [null]
    })
  }
  private addstudentsGroup(): FormGroup {
    return this.fb.group({
      studFName:[null,[Validators.required]],
        studLName:[null,[Validators.required]],
        studEmail:[null,[Validators.required,EmailValidation]],
        studPhone:[null,[Validators.required,mobileValidation]],
        studAdd:[null,[Validators.required]]
    })
  }
  
  // ADD Click event of plantsArray Form Array
  addGSTNArr() {
    let control = <FormArray>this.createCompany.controls.plants;
    control.push(
      this.fb.group({
        cmpCode: [null],
        plantCode: [null, [Validators.required]],
        plantName: [null, [Validators.required]],
        shortKey: [null, [Validators.required]],
        gstNumber: [null, [Validators.required, gstinValidation]],
        addressLine1: [null],
        addressLine2: [null],
        street: [null, [Validators.required]],
        city: [null, [Validators.required]],
        postalCode: [null, [Validators.required]],
        stateCode: [null, [Validators.required]],
        countryCode: [null, [Validators.required]],
        email: [null, [Validators.required, EmailValidation]],
        phone: [null, [Validators.required, mobileValidation]],
        createBy: [null],
        createOn: [null],
        updateBy: [null],
        updateOn: [null]
      })
    );
  }

  addStudDetArr(){
    let contro=<FormArray>this.createCompany.controls.students;
    contro.push(
      this.fb.group({
        studFName:[null,[Validators.required]],
        studLName:[null,[Validators.required]],
        studEmail:[null,[Validators.required,EmailValidation]],
        studPhone:[null,[Validators.required,mobileValidation]],
        studAdd:[null,[Validators.required]]
      })
    )
  }

  //Delete plantsArray Form array
  deletearr(i: any) {
    let control = <FormArray>this.createCompany.controls.plants;
    control.removeAt(i)
  }

  deleteArr(n:any)
  {
    let contro=<FormArray>this.createCompany.controls.students;
    contro.removeAt(n)
  }

  // company view table filter
  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // Datasource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }

  getCompany() {
    var data = {}
    this.apiCallService.getCompany(data).subscribe({
      next: (result: any) => {
        if (result) {
          this.CompanyList = result;
          console.log(result)
          // Assign the data to the data source for the table to render
          this.dataSource = new MatTableDataSource(this.CompanyList);
          this.dataSource.paginator = this.paginator;
          this.dataSource.sort = this.sort;
        }
      }, error: (error: any) => {
        this.commonService.showError(error.error);
      },
    })
  }

  getCountry() {
    var data = { param: null }
    this.apiCallService.getCountry(data).subscribe({
      next: (result: any) => {
        if (result) {
          this.CountryList = result;
        }
      }, error: (error: any) => {
        this.commonService.showError(error.error);
      }
    })
  }
  getState(CountryCode: any) {
    if (CountryCode) {
      var param = { CountryCode: CountryCode }
      var data = { param: param }
      this.apiCallService.getState(data).subscribe({
        next: (result: any) => {
          if (result) {
            this.StateList = result;
          }
        }, error: (error: any) => {
          this.commonService.showError(error.error);
        }
      })
    } else {
      this.StateList = [];
      this.createCompany.controls['stateCode'].setValue(null);
    }
  }

  addCompany() {
    if (this.createCompany.invalid) {
      this.submitted = true;
      return;
    }
    let Api = this.apiCallService.addCompany;
    var JSON = this.createCompany.value;
    JSON.createBy = "100"; // Loged IN User ID
    JSON.updateBy = "100"; // Loged IN User ID    
    // Convert base64 to binary
    JSON.cmpLogo = btoa(this.previews); // if image size more then 16 MB give error
    JSON.plants.find((item: any) => { item.CmpCode = JSON.cmpCode, item.CreateBy = JSON.createBy, item.UpdateBy = JSON.updateBy })
    if (this.isCreate == 1) { Api = this.apiCallService.updateCompany, JSON.oldCmpCode = this.updateRow.cmpCode }
    var data = { data: JSON }
    console.log(data, this.previews)
    Api(data).subscribe({
      next: (result: any) => {
        if (result) {
          this.commonService.showSuccess("Record Inserted Succefully!");
          this.createCompany.reset();
          this.removefile();
        }
      }, error: (error: any) => {
        if (error.status == 200) {
          this.commonService.showError(error.error.text);
          this.createCompany.reset();
          this.removefile();
        } else {
          this.commonService.showError(error.error);
        }
      }
    })


  }
  // Active Deactive matTab change event for Add method or update method
  tabIsActive(value: any, change: any) {
    if (value != change) { this.removefile(); this.updateRow = null, value = 0 }
    this.isActiveTab = value;
    this.isCreate = value;
    this.createCompany.reset();

  }
  editCompany(row: any) {
    this.updateRow = row;
    this.tabIsActive(1, 1);
    this.isCreate = 1;
    if (row.cmpLogo) { this.previews = atob(row.cmpLogo); this.selectedFileNames = "Logo.png"; row.cmpLogo = null }
    this.getState(row.countryCode)
    // Use add form for update patch values
    this.createCompany.patchValue(row)
  }
  viewCompany(row: any) {
    this.updateRow = row;
    this.openDialogWithTemplateRef(this.viewDialog);
    // Assign the data to the data source for the table to render
    this.viewDataSource = new MatTableDataSource(this.updateRow.plants);
  }
  deleteMe(row: any) {
    this.deleteRow = row;
    this.openDialogWithTemplateRef(this.deleteDialog);
  }
  deleteCompany() {
    this.deleteRow
    var data = { cmpCode: this.deleteRow.cmpCode }
    this.apiCallService.deleteCompany(data).subscribe({
      next: (result: any) => {
        if (result) {
          this.commonService.showSuccess("Record Deleted Succefully!");
          const index = this.CompanyList.indexOf(this.deleteRow);
          if (index > -1) { // only splice array when item is found
            this.CompanyList.splice(index, 1); // 2nd parameter means remove one item only
          }
          // Assign the data to the data source for the table to render
          this.dataSource = new MatTableDataSource(this.CompanyList);
        }
      }, error: (error: any) => {
        this.commonService.showError(error.error);
      }
    })
  }
  selectFiles(event: any): void {
    this.selectedFileNames = null;
    this.previews = null;
    this.selectedFiles = event.target.files;
    if (this.selectedFiles && this.selectedFiles[0]) {
      const numberOfFiles = this.selectedFiles.length;
      for (let i = 0; i < numberOfFiles; i++) {
        const reader = new FileReader();

        reader.onload = (e: any) => {
          this.previews = e.target.result;
        };
        // Convert file to base64
        reader.readAsDataURL(this.selectedFiles[i]);

        this.selectedFileNames = this.selectedFiles[i].name;
      }
    }
  }
  removefile() {
    this.selectedFileNames = null;
    this.previews = null;
  }
}
export interface CmpData {
  cmpCode: string;
  stateCode: string;
  cmpName: string;
  shortKey: string;
  street: string;
  cmpPAN: string;
  cmpCIN: string;
  addressLine1: string;
  addressLine2: string;
  city: string;
  postalCode: string;
  cmpLogo: string;
  cmpWebSiteURL: string;
  email: string;
  phone: string;
  createBy: string;
  createOn: string;
  updateBy: string;
  updateOn: string;
}

export interface PlantData {
  cmpCode: string;
  plantCode: string;
  plantName: string;
  shortKey: string;
  street: string;
  addressLine1: string;
  addressLine2: string;
  city: string;
  postalCode: string;
  email: string;
  phone: string;
  createBy: string;
  createOn: string;
  updateBy: string;
  updateOn: string;
}