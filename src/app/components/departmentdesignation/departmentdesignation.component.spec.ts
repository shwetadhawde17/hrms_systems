import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DepartmentdesignationComponent } from './departmentdesignation.component';

describe('DepartmentdesignationComponent', () => {
  let component: DepartmentdesignationComponent;
  let fixture: ComponentFixture<DepartmentdesignationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DepartmentdesignationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DepartmentdesignationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
