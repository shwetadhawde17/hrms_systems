import { Component, OnInit } from '@angular/core';
import { FormBuilder,FormArray,FormControl,FormGroup ,Form,Validators} from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-departmentdesignation',
  templateUrl: './departmentdesignation.component.html',
  styleUrls: ['./departmentdesignation.component.scss']
})
export class DepartmentdesignationComponent implements OnInit {
  deptdesForm: FormGroup;

  constructor(private fb: FormBuilder) {
    this.deptdesForm = this.fb.group({
      deptid: [null, [Validators.required]],
      deptname: [null, [Validators.required]],
      deptcmpname: [null, [Validators.required]],
      deptmngname: [null, [Validators.required]],
     
    })
  }
  adddeptdes() {
    console.log("deptdesForm", this.deptdesForm?.value);
  }

  ngOnInit(): void {
  }

}
