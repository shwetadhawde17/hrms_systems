import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateconsultantComponent } from './createconsultant.component';

describe('CreateconsultantComponent', () => {
  let component: CreateconsultantComponent;
  let fixture: ComponentFixture<CreateconsultantComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CreateconsultantComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateconsultantComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
