import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators, FormArray } from "@angular/forms";
import { ApiCallService } from 'src/app/services/api-call.service';
import { CommonService } from 'src/app/services/common.service';
import { mobileValidation, EmailValidation, ZipValidation } from '../../services/validations';
import { ViewChild, AfterViewInit, TemplateRef } from '@angular/core';

import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort, SortDirection } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { head } from 'lodash';

@Component({
  selector: 'app-createconsultant',
  templateUrl: './createconsultant.component.html',
  styleUrls: ['./createconsultant.component.scss']
})
export class CreateconsultantComponent implements OnInit {
  createConsultant: FormGroup;
  submitted: any;
  CompanyList: any;
  ConsultantList:any;
  FilterCompanyList: any;
  PlantList: any;
  CountryList: any;
  FilterCountryList: any;
  StateList: any;
  FilterStateList: any;

  
  updateRow: any;
  deleteRow: any;
  isActiveTab: number = 0;
  isCreate: number = 0;
  displayedColumns = ['action',  'bpFName', 'pan', 'street', 'city', 'postalCode',  'email', 'phone',];

  
  dataSource!: MatTableDataSource<CmpData>;
  viewDataSource!: MatTableDataSource<PlantData>;
  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;
  // Create 100 CompanyList

  constructor(private dialog: MatDialog,private fb: FormBuilder, private apiCallService: ApiCallService, private commonService: CommonService) {
    this.createConsultant = this.fb.group({
      bpCode: [null, [Validators.required]],
      cmpCode: [null, [Validators.required]],
      plantCode: [null, [Validators.required]],
      actGroupCode: ["CON", [Validators.required]],
      bpFName: [null, [Validators.required]],
      bpMName: [null],
     bpLName: [null],
      shortKey: [null, [Validators.required]],
      pan: [null, [Validators.required]],
      gstNumber: [null, [Validators.required]],
      addressLine1: [null],
      addressLine2: [null],
      street: [null, [Validators.required]],
      city: [null, [Validators.required]],
      postalCode: [null, [Validators.required, ZipValidation]],
      email: [null, [Validators.required, EmailValidation]],
      phone: [null, [Validators.required, mobileValidation]],
      stateCode: [null, [Validators.required]],
      countryCode: [null, [Validators.required]],
      status: [1, [Validators.required]],
      createBy: [null],
      createOn: [null],
      updateBy: [null],
      updateOn: [null],
      isDeleted: [null],
      relationPersons: [null],        
    })
  }
  ngOnInit(): void {
    document.body.style.backgroundColor = "#E5E7E9";
    this.getCompany();
    this.getCountry();
    this.getConsultant();
  }
 // Active Deactive matTab change event for Add method or update method
 tabIsActive(value: any, change: any) {
  if (value != change) { this.updateRow = null, value = 0 }
  this.isActiveTab = value;
  this.isCreate = value;
  this.createConsultant.reset();
}
  // Mat Dialogs.
  @ViewChild('viewDialog') viewDialog!: TemplateRef<any>;
  @ViewChild('deleteDialog') deleteDialog!: TemplateRef<any>;
  openDialogWithTemplateRef(templateRef: TemplateRef<any>) {
    this.dialog.open(templateRef);
  }

editConsultant(row: any) {
  this.updateRow = row;
  this.tabIsActive(1, 1);
  // Use add form for update patch values
  this.createConsultant.patchValue(row)
  
}

viewConsultant(row: any) {
  this.updateRow = row;
  this.openDialogWithTemplateRef(this.viewDialog);
  // // Assign the data to the data source for the table to render
  this.viewDataSource = new MatTableDataSource(this.updateRow.plants);
}
deleteCon(row: any) {
  this.deleteRow = row;
  console.log('delete',this.deleteRow)
  this.openDialogWithTemplateRef(this.deleteDialog);
}


 
  public myError = (controlName: string, errorName: string) => {
    return this.createConsultant.controls[controlName].hasError(errorName);
  }

  changeState(event: any) {
    if (event.target.value)
      this.FilterStateList = this._filterState(event.target.value);
    else
      this.FilterStateList = this.StateList;
  }
  private _filterState(value: string): string[] {
    const filterValue = value.toLowerCase();
    return this.StateList.filter((item: any) => item.stateCode.toLowerCase().includes(filterValue) || item.stateName.toLowerCase().includes(filterValue));
  }

  getConsultant() {
    var param = { Cid: "1000", Pid: "1010", GP: "CON" }
    var data = { param: param }
    this.apiCallService.getBusinessPartnerByCompPlant(data).subscribe({
      next: (result: any) => {
        if (result) {
          this.ConsultantList = result;
          console.log(result)
          // Assign the data to the data source for the table to render
          this.dataSource = new MatTableDataSource(this.ConsultantList);
          this.dataSource.paginator = this.paginator;
          this.dataSource.sort = this.sort;
        }
      }, error: (error: any) => {
        this.commonService.showError(error.error);
      },

    })

  }
  getCompany() {
    var data = {}
    this.apiCallService.getCompany(data).subscribe({
      next: (result: any) => {
        if (result) {
          this.CompanyList = result;
          this.FilterCompanyList = this.CompanyList;
        }
      }, error: (error: any) => {
        this.commonService.showError(error.error);
      }
    })
  }
  getCountry() {
    var data = {}
    this.apiCallService.getCountry(data).subscribe({
      next: (result: any) => {
        if (result) {
          console.log(result)
          this.CountryList = result;
          this.FilterCountryList = this.CountryList;
        }
      }, error: (error: any) => {
        this.commonService.showError(error.error);
      }
    })
  }
  getState(CountryCode: any) {
    if (CountryCode) {
      var param = { CountryCode: CountryCode }
      var data = { param: param }
      this.apiCallService.getState(data).subscribe({
        next: (result: any) => {
          if (result) {
            this.StateList = result;
          }
        }, error: (error: any) => {
          this.commonService.showError(error.error);
        }
      })
    } else {
      this.StateList = [];
      this.createConsultant.controls['stateCode'].setValue(null);
    }
  }
  getPlant(cmpCode: any) {
    if (cmpCode) {
      var data = { cmpCode: cmpCode }
      this.apiCallService.getPlantId(data).subscribe({
        next: (result: any) => {
          if (result) {
            this.PlantList = result;
          }
        }, error: (error: any) => {
          this.commonService.showError(error.error);
        }
      })
    } else {
      this.PlantList = [];
      this.createConsultant.controls['plantCode'].setValue(null);
    }
  }
  deleteConsultant() {
    this.deleteRow;
    console.log('delete API',this.deleteRow);
    // var head= {bpCode:this.deleteRow.bpCode}
    var head = { cmpCode:this.deleteRow.cmpCode, bpCode:this.deleteRow.bpCode}
    var data={head:head}
    console.log('......',data)
    this.apiCallService.deleteBusinessPartner(data).subscribe({
      next: (result: any) => {
        if (result) {
          this.commonService.showSuccess("Record Deleted Succefully!");
          const index = this.ConsultantList.indexOf(this.deleteRow);
          if (index > -1) { // only splice array when item is found
            this.ConsultantList.splice(index, 1); // 2nd parameter means remove one item only
          }
          // Assign the data to the data source for the table to render
          this.dataSource = new MatTableDataSource(this.ConsultantList);
        }
      }, error: (error: any) => {
        this.commonService.showError(error.error);
      }
    })
  }
  addConsultant() {
    console.log(this.createConsultant)
    if (this.createConsultant.invalid) {
      this.submitted = true;
      return;
    }
    let Api = this.apiCallService.addBusinessPartner;
    console.log(this.isCreate)
    var JSON = this.createConsultant.value;
    JSON.createBy = "100"; // Loged IN User ID
    JSON.updateBy = "100"; // Loged IN User ID
    JSON.relationPersons=[]
    var head= {}
    if (this.isCreate == 1) { Api = this.apiCallService.updateBusinessPartner, head= {cmpCode:this.updateRow.cmpCode,bpCode:this.updateRow.bpCode} } 
    var data = { data: JSON,head:head }
    
    console.log("ghfhfh",data)
  
    Api(data).subscribe({
      next: (result: any) => {
        if (result) {
          this.commonService.showSuccess("Record Inserted Succefully!");
          this.createConsultant.reset();
        }
      }, error: (error: any) => {
        if (error.status == 200) {
          this.commonService.showError(error.error.text);
          this.createConsultant.reset();
        } else {
          console.log(error)
          this.commonService.showError(error.error);
        }
      }
    })


  }
  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // Datasource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }
 
}


export interface CmpData {
  bpCode: string;
  cmpCode: string;
  plantCode: string;
  // actGroupCode: string;
  bpFName: string;
  shortKey: string;
  pan: string;
  gstNumber: string;
  addressLine1:string;
  addressLine2: string;
  street: string;
  city:string;
  postalCode:string;
  email: string;
  phone: string;
  stateCode: string;
  countryCode: string;
  status:string;
  createBy: string;
  createOn: string;
  updateBy: string;
  updateOn: string;

}

export interface PlantData {
  cmpCode: string;
  plantCode: string;
  plantName: string;
  shortKey: string;
  street: string;
  addressLine1: string;
  addressLine2: string;
  city: string;
  postalCode: string;
  email: string;
  phone: string;
  createBy: string;
  createOn: string;
  updateBy: string;
  updateOn: string;
}

