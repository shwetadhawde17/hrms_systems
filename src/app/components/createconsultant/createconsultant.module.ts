import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CreateconsultantRoutingModule } from './createconsultant-routing.module';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    CreateconsultantRoutingModule
  ]
})
export class CreateconsultantModule { }
