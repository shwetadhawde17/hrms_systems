import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { data } from 'jquery';
import { result } from 'lodash';
import { ApiCallService } from 'src/app/services/api-call.service';
import { CommonService } from 'src/app/services/common.service';

@Component({
  selector: 'app-projectmaster',
  templateUrl: './projectmaster.component.html',
  styleUrls: ['./projectmaster.component.scss']
})
export class ProjectmasterComponent implements OnInit {
  master: FormGroup
  submitted: any;
  cmpCodeList: any;
  CompanyList: any;
  masterDataType: any;
  isActiveTab: number = 0;
  updateRow: null | undefined;
  isCreate: number=0;
  ELEMENT_DATA:any;
     // View dataTabale
  displayedColumns: string[] = ['position', 'name', 'weight', 'symbol'];
  dataSource = new MatTableDataSource<PeriodicElement>(ELEMENT_DATA);
  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
  }
  constructor(private fb: FormBuilder,private apiCallService:ApiCallService,private commonService: CommonService) {
    this.master = this.fb.group({
      cmpCode:[null,[Validators.required]],
      masterDataType: [null, [Validators.required]]
    })
  }
 
  ngOnInit(): void {
this.getCompany();

  }
  

  // add remove input field 
  execution() {
    if (this.master.value.masterDataType == 'projectExecution') {
      // this.master.addControl('cmpCode', this.fb.control('', [Validators.required]));
      this.master.addControl('executionName', this.fb.control('', [Validators.required]));
      this.master.removeControl('projectTypeName');
      this.master.removeControl('DesignationCode');
      this.master.removeControl('Designation');
      this.master.removeControl('status');
    }
    else if (this.master.value.masterDataType == 'projectType') {
      // this.master.addControl('cmpCode', this.fb.control('', [Validators.required]))
      this.master.addControl('projectTypeName', this.fb.control('', [Validators.required]));
      this.master.removeControl('executionName');
      this.master.removeControl('DesignationCode');
      this.master.removeControl('Designation');
      this.master.removeControl('status');
    }
    else {
      // this.master.addControl('cmpCode', this.fb.control('', [Validators.required]));
      this.master.addControl('DesignationCode', this.fb.control('', [Validators.required]));
      this.master.addControl('Designation', this.fb.control('', [Validators.required]));
      this.master.addControl('status', this.fb.control('', [Validators.required]));
      this.master.removeControl('executionName');
      this.master.removeControl('projectTypeName');

    }
  }


  // masterData = [
  //   { name: 'Project Execution'},
  //   { name: 'Project Type'},
  //   { name: 'Project Designation'}
  // ];



   // Active Deactive matTab change event for Add method or update method
   tabIsActive(value: any, change: any) {
    if (value != change) { this.updateRow = null, value = 0 }
    this.isActiveTab = value;
    this.isCreate = value;
  }
  // editCompany(row: any) {
  //   this.updateRow = row;
  //   this.tabIsActive(1, 1);
  //   // Use add form for update patch values
  //   this.createCompany.patchValue(row)
  // }
  // viewCompany(row: any) {
  //   this.updateRow = row;
  //   this.openDialogWithTemplateRef(this.viewDialog);
  //   // Assign the data to the data source for the table to render
  //   this.viewDataSource = new MatTableDataSource(this.updateRow.plants);
  // }

    //  view table filter
    applyFilter(filterValue: string) {
      filterValue = filterValue.trim(); // Remove whitespace
      filterValue = filterValue.toLowerCase(); // Datasource defaults to lowercase matches
      this.dataSource.filter = filterValue;
    }
  // get Company Code 
  getCompany() {
             var data = { }
          this.apiCallService.getCompany(data).subscribe({
            next: (result: any) => {
              if (result) {
                this.CompanyList = result;
              }
            }, error: (error: any) => {
              this.commonService.showError(error.error);
            }
          })
        }

  
  // Get API

  

  // post API
  addmasterdata() {
    if (this.master.valid) {
        console.log(this.master.value)
        if (this.master.invalid) {
          this.submitted = true;
          return;
        }
      let Api = this.apiCallService.executionMaster;
      var JSON = this.master.value;
      var data = { data: JSON }
      Api(data).subscribe({
        next: (result: any) => {
          if (result) {
            this.commonService.showSuccess("Record Inserted Succefully!");
            this.master.reset();
          }
        }, error: (error: any) => {
          if (error.status == 200) {
            this.commonService.showError(error.error.text);
            this.master.reset();
          } else {
            console.log(error)
            this.commonService.showError(error.error);
          }
        }
      })
    }
  }
    // else {
    //   console.log("fill the master data")
    // }
    
  }
  export interface PeriodicElement {
    name: string;
    position: number;
    weight: number;
    symbol: string;
  }
  
  const ELEMENT_DATA: PeriodicElement[] = [
    {position: 1, name: 'Hydrogen', weight: 1.0079, symbol: 'H'},
    {position: 2, name: 'Helium', weight: 4.0026, symbol: 'He'},
    {position: 3, name: 'Lithium', weight: 6.941, symbol: 'Li'},
    {position: 4, name: 'Beryllium', weight: 9.0122, symbol: 'Be'},
    {position: 5, name: 'Boron', weight: 10.811, symbol: 'B'},
    {position: 6, name: 'Carbon', weight: 12.0107, symbol: 'C'},
    {position: 7, name: 'Nitrogen', weight: 14.0067, symbol: 'N'},
    {position: 8, name: 'Oxygen', weight: 15.9994, symbol: 'O'},
    {position: 9, name: 'Fluorine', weight: 18.9984, symbol: 'F'},
    {position: 10, name: 'Neon', weight: 20.1797, symbol: 'Ne'},
    {position: 11, name: 'Sodium', weight: 22.9897, symbol: 'Na'},
    {position: 12, name: 'Magnesium', weight: 24.305, symbol: 'Mg'},
    {position: 13, name: 'Aluminum', weight: 26.9815, symbol: 'Al'},
    {position: 14, name: 'Silicon', weight: 28.0855, symbol: 'Si'},
    {position: 15, name: 'Phosphorus', weight: 30.9738, symbol: 'P'},
    {position: 16, name: 'Sulfur', weight: 32.065, symbol: 'S'},
    {position: 17, name: 'Chlorine', weight: 35.453, symbol: 'Cl'},
    {position: 18, name: 'Argon', weight: 39.948, symbol: 'Ar'},
    {position: 19, name: 'Potassium', weight: 39.0983, symbol: 'K'},
    {position: 20, name: 'Calcium', weight: 40.078, symbol: 'Ca'},
  ];