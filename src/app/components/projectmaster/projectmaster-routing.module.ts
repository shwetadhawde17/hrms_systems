import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ProjectmasterComponent } from './projectmaster.component';

const routes: Routes = [{path:'',component:ProjectmasterComponent,children:[]}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProjectmasterRoutingModule { }
