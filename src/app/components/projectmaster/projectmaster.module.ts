import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProjectmasterRoutingModule } from './projectmaster-routing.module';
import { MatPaginator, MatPaginatorModule } from '@angular/material/paginator';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    ProjectmasterRoutingModule,
    MatPaginatorModule
  ]
})
export class ProjectmasterModule { }
