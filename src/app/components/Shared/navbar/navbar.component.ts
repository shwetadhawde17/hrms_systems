import { Component, ViewChild, HostListener,EventEmitter,OnInit ,Output } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import {MatSidenav} from '@angular/material/sidenav';
import { ActivatedRoute, Router } from '@angular/router';
import screenfull from 'screenfull';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})


export class NavbarComponent implements OnInit {
  

  @Output() public sidenavToggle = new EventEmitter();
  isExpanded= true;
  hide = true;
  menus: any;
  screenWidth?: number;
  private screenWidth$ = new BehaviorSubject<number>(window.innerWidth);
  @ViewChild('sidenav') sidenav?: MatSidenav;
  @HostListener('window:resize', ['$event'])
  onResize(event:any) {
    this.screenWidth$.next(event.target.innerWidth);
  }
  constructor(private router: Router) { 
    this.menus = [
      {
        "name": 'Dashboard',
        "href": '/components/dashboard',
        "icons":'dashboard'

      },
      {
        "name": 'Company ',
        "href": '/components/createcompany',
        "icons":'business'

      },
     
      {
        "name": 'Client ',
        "href": '/components/createclient',
        "icons":'person_outline'

      },
      {
        "name": 'Employee ',
        "href": '/components/createemployee',
        "icons":'person'

      },
      {
        "name": 'Project Creation',
        "href": '/components/projectcreation',
        "icons":'dvr'

      },

     
      {
        "name":'Project Master',
        "href":'/components/projectmaster',
        "icons":"developer_board"

      },
      {
        "name":'Data Master',
        // "href":'/components/datamaster',
        "icons":'layers'

      },
      {
      "text": "Data Master",
      "icon": "layers",
      "children": [
        {
          "text": 'Project Designation',
          "routerLink": '/components/projectdesignation',
          "icon": "dvr"

        },
        {
          "text": 'Employee Designation',
          "routerLink": '/components/employeedesignation/employeedesignation.component',
          "icon": 'working'
        },
        {
          "text": 'Department Designation',
          "routerLink": '/components/departmentdesignation/departmentdesignation.component',
          "icon": 'ballot'
        }
      ]
      }
    ]
  }

  ngOnInit(): void {
    this.screenWidth$.subscribe(width => {
      this.screenWidth = width;
    });
  }

  logout()
  {
    sessionStorage.clear();
    this.router.navigate(['login']);
  }
  toggleFullscreen()
  {
    
    var x=document.getElementById("");
      if (screenfull.isEnabled ) {
        screenfull.toggle();
        this.hide = !this.hide

      } 
  }

  

}