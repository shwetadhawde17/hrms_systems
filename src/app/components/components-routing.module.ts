import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ComponentsComponent } from './components.component';

import { ProjectdesignationComponent } from './projectdesignation/projectdesignation.component';
import { DepartmentdesignationComponent } from './departmentdesignation/departmentdesignation.component';
import { EmployeedesignationComponent } from './employeedesignation/employeedesignation.component';

const routes: Routes = [

  {

    path: '', component: ComponentsComponent, children: [
      {
        path: '',
        redirectTo: 'dashboard',
        pathMatch: 'full'
      },

      {
        path: 'dashboard',
        loadChildren: () => import('./dashboard/dashboard.module').then(
          module => module.DashboardModule
        )
      },
      {
        path: 'createcompany',
        loadChildren: () => import('./createcompany/createcompany.module').then(
          module => module.CreatecompanyModule
        )
      },
    

      {
        path: 'createclient',
        loadChildren: () => import('./createclient/createclient.module').then(
          module => module.CreateclientModule
        )
      },
      {
        path: 'createconsultant',
        loadChildren: () => import('./createconsultant/createconsultant.module').then(
          module => module.CreateconsultantModule
        )
      },
      {
        path: 'createemployee',
        loadChildren: () => import('./createemployee/createemployee.module').then(
          module => module.CreateemployeeModule
        )
      },
      {
        path: 'projectcreation',
        loadChildren: () => import('./projectcreation/projectcreation.module').then(
          module => module.ProjectcreationModule
        )
      },
    
      {
        path:'projectmaster',
        loadChildren: () => import('./projectmaster/projectmaster.module').then(
          module => module.ProjectmasterModule
        )
      },
      {
        path:'datamaster',
        loadChildren:() => import('./datamaster/datamaster.module').then(
          module => module.DatamasterModule
        )
      },
      {
        path: 'projectdesignation',component:ProjectdesignationComponent
        
      },
      {
        path: 'employeedesignation',component:EmployeedesignationComponent
        
      },
      {
        path: 'departmentdesignation',component:DepartmentdesignationComponent
        
      },
    ]
  }

];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ComponentsRoutingModule { }
