import { state } from '@angular/animations';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators, FormArray } from "@angular/forms";

import { ApiCallService } from 'src/app/services/api-call.service';
import { CommonService } from 'src/app/services/common.service';

import { ViewChild, AfterViewInit, TemplateRef } from '@angular/core';

import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort, SortDirection } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { mobileValidation, EmailValidation, panValidation,ZipValidation, gstinValidation, IFSCValidation,Accountnumber } from '../../services/validations';
@Component({
  selector: 'app-vendorcreation',
  templateUrl: './vendorcreation.component.html',
  styleUrls: ['./vendorcreation.component.scss']
})

export class VendorcreationComponent implements OnInit {
  createVendor: FormGroup;
 val:any;
  isActiveTab: any;
  updateRow: any;
  isCreate: any;
  submitted: any;
  constructor(private dialog: MatDialog,private fb: FormBuilder, private apiCallService: ApiCallService, private commonService: CommonService ) {
    this.createVendor = this.fb.group({
      Category: [null, [Validators.required]],
      Name: [null, [Validators.required]],
      NatureOfWork: [null, [Validators.required]],
      Country: [null, [Validators.required]],
      State: [null, [Validators.required]],
      City: [null, [Validators.required]],
      Street: [null, [Validators.required]],
      AddressLine1: '',
      AddressLine2: '',
      PostalCode: [null, [Validators.required, ZipValidation]],
      ContactPerson: [null, [Validators.required]],
      Phone: [null, [Validators.required, mobileValidation]],
      Email: [null, [Validators.required, EmailValidation]],
      PAN: [null, [Validators.required, panValidation]],
      BankersName: [null, [Validators.required]],
      BranchAddress: [null, [Validators.required]],
      ACNo: [null, [Validators.required,Accountnumber]],
      MICRcode: [null, [Validators.required]],
      ISFC: [null, [Validators.required,IFSCValidation]],
      GSTINIS: [null, [Validators.required]],
      // StateGSTIN: [null, [Validators.required,]],
      // GSTIN: [null, [Validators.required, gstinValidation]],
      // RegisterAddress: [null, [Validators.required]],
      // Type: [null, [Validators.required]],
      // CityGSTIN: [null, [Validators.required]],
      // CountryGSTIN: [null, [Validators.required]],
      // PostalCodeGSTIN: [null, [Validators.required, ZipValidation]],
      // ContactPersonGSTIN: [null, [Validators.required]],
      // PhoneGSTIN: [null, [Validators.required, mobileValidation]],
      // EmailGSTIN: [null, [Validators.required, EmailValidation]],
    })
  }
  public myError = (controlName: string, errorName: string) => {
    return this.createVendor.controls[controlName].hasError(errorName);
  }
  
  
  ngOnInit(): void {
   
  }
 // Active Deactive matTab change event for Add method or update method
 tabIsActive(value: any, change: any) {
  if (value != change) { this.updateRow = null, value = 0 }
  this.isActiveTab = value;
  this.isCreate = value;
  this.createVendor.reset();
}
  get StateGSTIN() {
    return this.createVendor.get('StateGSTIN') as FormControl;
  }
  onSelectEvent(){
    if(this.createVendor.value.GSTINIS=='Yes'){
      this.createVendor.addControl('StateGSTIN',this.fb.control('',[Validators.required]));
      this.createVendor.addControl('GSTIN',this.fb.control('',[Validators.required,gstinValidation]));
      this.createVendor.addControl('RegisterAddress',this.fb.control('',[Validators.required]));
      this.createVendor.addControl('Type',this.fb.control('',[Validators.required]));
      this.createVendor.addControl('CityGSTIN',this.fb.control('',[Validators.required]));
      this.createVendor.addControl('CountryGSTIN',this.fb.control('',[Validators.required]));
      this.createVendor.addControl('PostalCodeGSTIN',this.fb.control('',[Validators.required,ZipValidation]));
      this.createVendor.addControl('ContactPersonGSTIN',this.fb.control('',[Validators.required]));
      this.createVendor.addControl('PhoneGSTIN',this.fb.control('',[Validators.required,mobileValidation]));
      this.createVendor.addControl('EmailGSTIN',this.fb.control('',[Validators.required,EmailValidation]));

    }else{
      this.createVendor.removeControl('StateGSTIN');
      this.createVendor.removeControl('GSTIN');
      this.createVendor.removeControl('RegisterAddress');
      this.createVendor.removeControl('Type');
      this.createVendor.removeControl('CityGSTIN');
      this.createVendor.removeControl('CountryGSTIN');
      this.createVendor.removeControl('PostalCodeGSTIN');
      this.createVendor.removeControl('ContactPersonGSTIN');
      this.createVendor.removeControl('PhoneGSTIN');
      this.createVendor.removeControl('EmailGSTIN');
    }
  }
  
  onSelectContractor(){
    if(this.createVendor.value.Category=='Contractor'){
      this.createVendor.addControl('contractorType',this.fb.control('',[Validators.required]));
    }else{
      this.createVendor.removeControl('contractorType');
    }
  }

  addVendor() {
    console.log(this.createVendor)
    if (this.createVendor.invalid) {
      this.submitted = true;
      return;
    }
  //   let Api = this.apiCallService.addBusinessPartner;
  //   console.log(this.isCreate)
  //   var JSON = this.createVendor.value;
  //   JSON.createBy = "100"; // Loged IN User ID
  //   JSON.updateBy = "100"; // Loged IN User ID
  //   JSON.relationPersons=[]
  //   var head= {}
  //   if (this.isCreate == 1) { Api = this.apiCallService.updateBusinessPartner, head= {cmpCode:this.updateRow.cmpCode,bpCode:this.updateRow.bpCode} } 
  //   var data = { data: JSON,head:head }
    
  //   console.log("ghfhfh",data)
  
  //   Api(data).subscribe({
  //     next: (result: any) => {
  //       if (result) {
  //         this.commonService.showSuccess("Record Inserted Succefully!");
  //         this.createVendor.reset();
  //       }
  //     }, error: (error: any) => {
  //       if (error.status == 200) {
  //         this.commonService.showError(error.error.text);
  //         this.createVendor.reset();
  //       } else {
  //         console.log(error)
  //         this.commonService.showError(error.error);
  //       }
  //     }
  //   })


  }
}
