import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VendorcreationComponent } from './vendorcreation.component';

describe('VendorcreationComponent', () => {
  let component: VendorcreationComponent;
  let fixture: ComponentFixture<VendorcreationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ VendorcreationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(VendorcreationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
