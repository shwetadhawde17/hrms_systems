import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { VendorcreationRoutingModule } from './vendorcreation-routing.module';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    VendorcreationRoutingModule
  ]
})
export class VendorcreationModule { }
