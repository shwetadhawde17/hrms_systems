import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProjectcreationRoutingModule } from './projectcreation-routing.module';

import { FormsModule,ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,FormsModule,ReactiveFormsModule,
    ProjectcreationRoutingModule
  ]
})
export class ProjectcreationModule { }
