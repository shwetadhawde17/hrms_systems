import { Component, OnInit, ViewChild } from '@angular/core';
import { TemplateRef } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { FormGroup, FormControl, FormBuilder, Validators, FormArray } from "@angular/forms";
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { mobileValidation, EmailValidation, ZipValidation } from '../../services/validations';
import { CreateclientComponent } from '../createclient/createclient.component';
import { ApiCallService } from 'src/app/services/api-call.service';
import { CommonService } from 'src/app/services/common.service';

import { MatPaginator } from '@angular/material/paginator';
import { MatSort, SortDirection } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
@Component({
  selector: 'app-projectcreation',
  templateUrl: './projectcreation.component.html',
  styleUrls: ['./projectcreation.component.scss']
})
export class ProjectcreationComponent implements OnInit {

  createProject: FormGroup;
  createPerson: FormGroup;
  submitted: any;
  currentIndex: any;
  CompanyList: any;
  ExecutaionList: any;
  projectTypeList:any;
  PlantList: any;
  CountryList: any;
  FilterCompanyList:any;
  FilterCountryList: any;
  StateList: any;
  FilterStateList: any;
 PrjList: any;
  FilterPrjList: any;
  options: string[] = ['India'];
  filteredOptions?: Observable<string[]>;
  designationList: any;
  bpCodeList: any;

  displayProjectColumns= ['action','projName','executaionCode','projectTypeCode','addressLine1', 'addressLine2','street', 'city','postalCode','projectCost', 'projectEndDate', ];
  clientCodeList: any;
  isActiveTab: any;
  isCreate: any;
  updateRow: any;

 
  constructor(private fb: FormBuilder, private dialog: MatDialog, private apiCallService: ApiCallService, private commonService: CommonService) {
    this.createPerson = this.fb.group({
      persontype: [null, [Validators.required]],
      name: [null, [Validators.required]],
      phone: [null, [Validators.required, mobileValidation]],
      emailId: [null, [Validators.required, EmailValidation]],
    })
    //Project Creation Form Group
    this.createProject = this.fb.group({
      projCode: 111,
      cmpCode: [null, [Validators.required]],
      plantCode: [null, [Validators.required]],
      executaionCode: [null, [Validators.required]],
      projectTypeCode: [null, [Validators.required]],
      projName: [null, [Validators.required]],
      projDesc: [null, [Validators.required]],
      addressLine1: [null, [Validators.required]],
      addressLine2: [null, [Validators.required]],
      street: [null, [Validators.required]],
      city: [null, [Validators.required]],
      postalCode: [null, [Validators.required, ZipValidation]],
      stateCode: [null, [Validators.required]],
      countryCode: [null, [Validators.required]],
      projectCost: [null, [Validators.required]],
      projectEndDate: [null, [Validators.required]],
      projectStatus: 1,
      clientCode: [null, [Validators.required]],
      consultantCode: 200,
      createBy: 100,
      createOn: null,
      updateBy: 100,
      updateOn: null,
      
      // block Array 
      blockArray: new FormArray([this.BlockdataGroup()]),
      //store Array
      storeArray: new FormArray([this.StoredataGroup()]),
      //Key Person Array
      keyPersonArray: new FormArray([this.keyPersonGroup()]),
      //Client contact details array 
      clientContactDetailArray: new FormArray([this.clientDataGroup()]),

    })
  }
  dataSource!: MatTableDataSource<PrjData>;
  // viewDataSource!: MatTableDataSource<PlantData>;
  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;
  @ViewChild('clientDialog') clientDialog!: TemplateRef<any>;
  openDialogWithTemplateRef(templateRef: TemplateRef<any>) {
    this.dialog.open(templateRef);
    // this.dialogReff.close(templateRef)
    
   
  }
//   closeDialog(){
//     this.dialogReff.close(); // <- Closes the dialog box
// }
  openModal() {
    this.openDialogWithTemplateRef(this.clientDialog);
  }
  ngOnInit(): void {
 
    this.getCompany();
    this.getCountry();
   
    this.getExecutaionList();
    this.getprojectTypeList();
    this.getdesignationList();
    this.getbpCodeList();
    this.getclientCodeList();  
  }

   // Active Deactive matTab change event for Add method or update method
 tabIsActive(value: any, change: any) {
  if (value != change) { this.updateRow = null, value = 0 }
  this.isActiveTab = value;
  this.isCreate = value;
  this.createProject.reset();
}
  getCompany() {
    var data = {}
    this.apiCallService.getCompany(data).subscribe({
      next: (result: any) => {
        if (result) {
          this.CompanyList = result;
          this.FilterCompanyList = this.CompanyList;
        }
      }, error: (error: any) => {
        this.commonService.showError(error.error);
      }
    })
  }
  getPlant(cmpCode: any) {
    if (cmpCode) {
      var data = { cmpCode: cmpCode }
      this.apiCallService.getPlantId(data).subscribe({
        next: (result: any) => {
          if (result) {
            this.PlantList = result;
          }
        }, error: (error: any) => {
          this.commonService.showError(error.error);
        }
      })
    } else {
      this.PlantList = [];
      this.createProject.controls['plantCode'].setValue(null);
    }
  }
  //ExecutaionList Add api***
  getExecutaionList() {
    var data = {}
    this.apiCallService.executionMaster(data).subscribe({
      next: (result: any) => {
        if (result) {
          console.log(result)
          this.ExecutaionList = result;
         
        }
      }, error: (error: any) => {
        this.commonService.showError(error.error);
      }
    })
  }
//getprojectTypeLis  Add api*****/
  getprojectTypeList() {
    var data = {}
    this.apiCallService.getProjectType(data).subscribe({
      next: (result: any) => {
        if (result) {
          console.log(result)
          this.projectTypeList = result; 
        }
      }, error: (error: any) => {
        this.commonService.showError(error.error);
      }
    })
  }

  getCountry() {
    var data = {}
    this.apiCallService.getCountry(data).subscribe({
      next: (result: any) => {
        if (result) {
          console.log(result)
          this.CountryList = result;
          this.FilterCountryList = this.CountryList;
        }
      }, error: (error: any) => {
        this.commonService.showError(error.error);
      }
    })
  }

  getState(CountryCode: any) {
    if (CountryCode) {
      var param = { CountryCode: CountryCode }
      var data = { param: param }
      this.apiCallService.getState(data).subscribe({
        next: (result: any) => {
          if (result) {
            this.StateList = result;
          }
        }, error: (error: any) => {
          this.commonService.showError(error.error);
        }
      })
    } else {
      this.StateList = [];
      this.createProject.controls['stateCode'].setValue(null);
    }
  }

  getclientCodeList() {
    var data = {}
    this.apiCallService.getProjectType(data).subscribe({
      next: (result: any) => {
        if (result) {
          console.log(result)
          this.clientCodeList = result; 
        }
      }, error: (error: any) => {
        this.commonService.showError(error.error);
      }
    })
  }
 ///Add api
  getdesignationList() {
    var data = {}
    this.apiCallService.getProjectType(data).subscribe({
      next: (result: any) => {
        if (result) {
          console.log(result)
          this.designationList = result; 
        }
      }, error: (error: any) => {
        this.commonService.showError(error.error);
      }
    })
  }
 ///Add api bpCodeList

  getbpCodeList() {
    var data = {}
    this.apiCallService.getProjectType(data).subscribe({
      next: (result: any) => {
        if (result) {
          console.log(result)
          this.bpCodeList = result; 
        }
      }, error: (error: any) => {
        this.commonService.showError(error.error);
      }
    })
  }
  public myError = (controlName: string, errorName: string) => {
    return this.createProject.controls[controlName].hasError(errorName);
  }
  public Error = (controlName: string, errorName: string) => {
    return this.createPerson.controls[controlName].hasError(errorName);
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // Datasource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }

  //*************Block Section****************//
  //get Blocks Form Array
  get blockArrayError(): any {
    return <FormArray>this.createProject.get('blockArray') as FormArray;
  }
  public myErrorArray = (controlName: string, errorName: string, i: any) => {
    return this.blockArrayError['controls'][i]['controls'][controlName].hasError(errorName)
  }
  blocks(): FormArray {
    return <FormArray>this.createProject.get('blockArray') as FormArray;
  }
  //add Blocks Form Array
  addBlock() {
    this.blocks().push(this.BlockdataGroup());
  }
  //remove Blocks Form Array
  removeBlock(index: number) {
    this.blocks().removeAt(index);
  }
  //Blocks Form Array function
  private BlockdataGroup(): FormGroup {
    return this.fb.group({
      blockCode: 0,
      cmpCode: null,
      plantCode: null,
      projCode: null,
      blockName: new FormControl('', Validators.required),
      desc: new FormControl('', Validators.required),
      buildingArray: new FormArray([
        this.BuildingGroup()
      ])

    });
  }

  //*************Block Section END****************//

  //*************Building Section****************//
  //get Buildings Form Array
  get arrayErrorBuilding(): any {
    return <FormArray>this.createProject.get('buildingArray') as FormArray;
  }

  buildings(index: number): FormArray {
    return this.blocks().at(index).get('buildingArray') as FormArray;
  }
  //add Buildings Form Array
  addBuilding(index: number) {
    this.buildings(index).push(this.BuildingGroup());
  }
  //remove Building Form Array
  removeBuildingArray(i: number, j: number) {
    this.buildings(i).removeAt(j);
  }
  //Buildings Form Array function
  private BuildingGroup(): FormGroup {
    return this.fb.group({
      buildingName: new FormControl('', Validators.required),
      desc: new FormControl('', Validators.required),
      buildCode: 0,
      cmpCode: null,
      plantCode: null,
      projCode: null,
      blockCode: 0,
      FloorArray: new FormArray([
        this.FloorGroup()
      ])
    })
  }
  //*************Building Section END****************//

  //*************Floor Section****************//

  Floors(i: any, j: any): FormArray {
    // console.log('Floors....', this.buildings(i).at(j).get('FloorArray'));
    return this.buildings(i).at(j).get('FloorArray') as FormArray;
  }
  //add Buildings Form Array
  addFloor(i: number, k: any) {
    this.Floors(i, k).push(this.FloorGroup());
  }
  // //remove Building Form Array
  removeFloorArray(i: number, j: number, k: number) {
    this.Floors(i, j).removeAt(k);
  }
  //Buildings Form Array function
  private FloorGroup(): FormGroup {
    return this.fb.group({
      floorCode: null,
      buildCode: 0,
      cmpCode: 1000,
      plantCode: 1010,
      projCode: 111,
      blockCode: 0,
      floorName: new FormControl(''),
      floorDesc: new FormControl(''),
    })
  }
  //*************Floor Section END****************//

  //*************Store Section****************//
  get storeArrayError(): any {
    return <FormArray>this.createProject.get('storeArray') as FormArray;
  }
  public storeErrorArray = (controlName: string, errorName: string, i: any) => {
    return this.storeArrayError['controls'][i]['controls'][controlName].hasError(errorName)
  }
  //get Store Form Array
  stores(): FormArray {
    return <FormArray>this.createProject.get('storeArray') as FormArray;
  }
  // add store Form Array
  addStores() {
    this.stores().push(this.StoredataGroup());
  }
  //Store Form Array function
  StoredataGroup(): FormGroup {
    return this.fb.group({
      cmpCode: null,
      plantCode: null,
      projCode: null,
      storeCode: 0,
      storeName: new FormControl('', Validators.required),
      desc: new FormControl('', Validators.required),


      subStoreArray: new FormArray([
        // this.SubStoreGroup()
      ])
    });
  }
  //*************Store Section END****************//

  //*************Sub Store Section****************//
  //get substores Form Array
  subStores(index: number): FormArray {
    return this.stores().at(index).get('subStoreArray') as FormArray;
  }
  //add Sub store Form Array
  addSubStore(index: number) {
    this.subStores(index).push(this.SubStoreGroup());
  }
  //remove Sub store Form Array
  removeSubStores(i: number, j: number) {
    this.subStores(i).removeAt(j);
  }

  //Sub store Form Array function
  private SubStoreGroup(): FormGroup {
    return this.fb.group({
      cmpCode: null,
      plantCode: null,
      projCode: null,
      storeCode: 0,
      subStoreCode: 0,
      storeName: new FormControl('', Validators.required),
      desc: new FormControl('', Validators.required),
    })
  }
  //*************Store Section END****************//

  //*************Key Person Section****************//
  get keypersonArrayError(): any {
    return <FormArray>this.createProject.get('keyPersonArray') as FormArray;
  }
  public keyErrorArray = (controlName: string, errorName: string, i: any) => {
    return this.keypersonArrayError['controls'][i]['controls'][controlName].hasError(errorName)
  }
  //get addkeyPerson Form Array
  keyPerson(): FormArray {
    return <FormArray>this.createProject.get('keyPersonArray') as FormArray;
  }
  //add addkeyPerson Form Array
  addkeyPerson() {
    this.keyPerson().push(this.keyPersonGroup());
  }
  //remove Key person Form Array
  removekeyPerson(index: number) {
    this.keyPerson().removeAt(index);
  }
  //Key person Form Array function
  private keyPersonGroup(): FormGroup {
    return this.fb.group({
      cmpCode: 1000,
      plantCode: 1010,
      projCode: 111,
      designationCode: new FormControl('', Validators.required),
      bpCode: new FormControl('', Validators.required),
     

    });
  }
  //*************Key Person Section END****************//


  //*************Client Section****************//


  //get Store Form Array
  client(): FormArray {
    return <FormArray>this.createProject.get('clientContactDetailArray') as FormArray;
  }
  //Add client contact person
  addContactPerson() {
    this.client().push(this.clientDataGroup());
  }
  //Remove client contact person
  removeContactPerson(index: number) {
    this.client().removeAt(index);
  }
  //client contact person Form Array function
  clientDataGroup(): FormGroup {
    return this.fb.group({
      cmpCode: null,
      plantCode: null,
      projCode: null,
      cpRole: new FormControl('', Validators.required),
      bpCode: new FormControl('', Validators.required),
    });
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(CreateclientComponent, {
      width: '100%',
      disableClose: false
    });
    // this.closeDialog()
    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
    });
  }





  //*************CRUD Operation**************** 27AAJCG2948P1ZV//
 
  addProject() {
    console.log(this.createProject)
    if (this.createProject.invalid) {
      this.submitted = true;
      return;
    }
    let Api = this.apiCallService.addProject;
    console.log(this.isCreate)
    var JSON = this.createProject.value;
    JSON.createBy = "100"; // Loged IN User ID
    JSON.updateBy = "100"; // Loged IN User ID
    JSON.relationPersons=[]
    var head= {}
    if (this.isCreate == 1) { Api = this.apiCallService.updateProject, head= {cmpCode:this.updateRow.cmpCode,bpCode:this.updateRow.bpCode} } 
    var data = { data: JSON,head:head }
    
    console.log("Project form",data)
  
    Api(data).subscribe({
      next: (result: any) => {
        if (result) {
          this.commonService.showSuccess("Record Inserted Succefully!");
          this.createProject.reset();
        }
      }, error: (error: any) => {
        if (error.status == 200) {
          this.commonService.showError(error.error.text);
          this.createProject.reset();
        } else {
          console.log(error)
          this.commonService.showError(error.error);
        }
      }
    })


  }
  
  submitForm() { }

}

export interface PrjData 
{
  projCode: string;
  cmpCode: string;
  plantCode: string; 
  executaionCode: string; 
  projectTypeCode: string; 
  projName: string; 
  projDesc: string; 
  addressLine1: string; 
  addressLine2: string; 
  street: string;
  city: string;
  postalCode: string; 
  stateCode: string; 
  countryCode: string; 
  projectCost: string;
  projectEndDate: string;
  projectStatus: string;
  clientCode: string; 
  consultantCode: string; 
  createBy: string; 
  createOn: string; 
  updateBy: string; 
  updateOn: string; 
}