import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormArray, FormControl, FormGroup, Form, Validators } from '@angular/forms';
@Component({
  selector: 'app-projectdesignation',
  templateUrl: './projectdesignation.component.html',
  styleUrls: ['./projectdesignation.component.scss']
})
export class ProjectdesignationComponent implements OnInit {
  projdesForm: FormGroup;

  constructor(private fb: FormBuilder) {
    this.projdesForm = this.fb.group({
      projID: [null, [Validators.required]],
      projdesc: [null, [Validators.required]],
      projcliID: [null, [Validators.required]],
      projcliname: [null, [Validators.required]],
      projMngID: [null, [Validators.required]],
      projMngname: [null, [Validators.required]],
      projStartDate: [null, [Validators.required]],
      projEndDate: [null, [Validators.required]],
      projempId: [null, [Validators.required]],
      projempname: [null, [Validators.required]],
      projdeptname: [null, [Validators.required]],

    })
  }
  addprojdes() {
    console.log("compForm", this.projdesForm?.value);
  }

  ngOnInit(): void {
  }

}
