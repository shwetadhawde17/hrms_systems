import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProjectdesignationComponent } from './projectdesignation.component';

describe('ProjectdesignationComponent', () => {
  let component: ProjectdesignationComponent;
  let fixture: ComponentFixture<ProjectdesignationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ProjectdesignationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProjectdesignationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
