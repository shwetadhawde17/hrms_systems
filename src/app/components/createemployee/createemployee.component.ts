import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { MatDialog } from '@angular/material/dialog';
import { MatTableDataSource } from '@angular/material/table';
import { ApiCallService } from 'src/app/services/api-call.service';
import { CommonService } from 'src/app/services/common.service';
import { mobileValidation, EmailValidation, ZipValidation } from '../../services/validations';
@Component({
  selector: 'app-createemployee',
  templateUrl: './createemployee.component.html',
  styleUrls: ['./createemployee.component.scss']
})
export class CreateemployeeComponent implements OnInit {
  createEmployee: FormGroup;
  submitted: any;
  CompanyList: any;
  PlantList: any;
  CountryList: any;
  StateList: any;
  isActiveTab: number = 0;
  
  isCreate : number = 0;
  updateRow: any;
  displayedColumns = ['action', 'bpCode', 'bpFName', 'actGroupCode', 'shortKey', 'pan', 'gstNumber', 'street', 'city', 'postalCode', 'phone', 'email'];

  dataSource!: MatTableDataSource<BPEMPData>;
  paginator: any;
  sort: any;
  EmployeeList: any;
  // viewDataSource: MatTableDataSource<>;
  deleteRow: any;
  constructor(private dialog: MatDialog, private fb: FormBuilder, private apiCallService: ApiCallService, private commonService: CommonService) {
    this.createEmployee = this.fb.group({
      bpCode: [null, [Validators.required]],
      cmpCode: [null, [Validators.required]],
      plantCode: [null, [Validators.required]],
      actGroupCode: ["EMP", [Validators.required]],
      bpFName: [null, [Validators.required]],
      bpMName: [null, [Validators.required]],
      bpLName: [null, [Validators.required]],
      shortKey: [null, [Validators.required]],
      pan: [null, [Validators.required]],
      gstNumber: [null, [Validators.required]],
      addressLine1: [null],
      addressLine2: [null],
      street: [null, [Validators.required]],
      city: [null, [Validators.required]],
      postalCode: [null, [Validators.required, ZipValidation]],
      email: [null, [Validators.required, EmailValidation]],
      phone: [null, [Validators.required, mobileValidation]],
      stateCode: [null, [Validators.required]],
      countryCode: [null, [Validators.required]],
      status: [1, [Validators.required]],
      createBy: [null],
      createOn: [null],
      updateBy: [null],
      updateOn: [null],
      isDeleted: [null],
      relationPersons: [null],
    })
  }
  ngOnInit(): void {
    document.body.style.backgroundColor = "#E5E7E9";
    this.getCompany();
    this.getCountry();
    this.getEmployee();
  }
  public myError = (controlName: string, errorName: string) => {
    return this.createEmployee.controls[controlName].hasError(errorName);
  }

  // Mat Dialogs.
  @ViewChild('deleteDialog') deleteDialog!: TemplateRef<any>;
  openDialogWithTemplateRef(templateRef: TemplateRef<any>) {
    this.dialog.open(templateRef);
  }
  // Active Deactive matTab change event for Add method or update method
  tabIsActive(value: any, change: any) {
    if (value != change) { this.updateRow = null, value = 0 }
    this.isActiveTab = value;    
    this.isCreate = value;
    this.createEmployee.reset();
  }

  // Table view filter
  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // Datasource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }
  getCompany() {
    var data = {}
    this.apiCallService.getCompany(data).subscribe({
      next: (result: any) => {
        if (result) {
          this.CompanyList = result;
        }
      }, error: (error: any) => {
        this.commonService.showError(error.error);
      }
    })
  }
  getPlant(cmpCode: any) {
    if (cmpCode) {
      var data = { cmpCode: cmpCode }
      this.apiCallService.getPlantId(data).subscribe({
        next: (result: any) => {
          if (result) {
            this.PlantList = result;
          }
        }, error: (error: any) => {
          this.commonService.showError(error.error);
        }
      })
    } else {
      this.PlantList = [];
      this.createEmployee.controls['plantCode'].setValue(null);
    }
  }
  getCountry() {
    var data = { param: null }
    this.apiCallService.getCountry(data).subscribe({
      next: (result: any) => {
        if (result) {
          this.CountryList = result;
        }
      }, error: (error: any) => {
        this.commonService.showError(error.error);
      }
    })
  }
  getState(CountryCode: any) {
    if (CountryCode) {
      var param = { CountryCode: CountryCode }
      var data = { param: param }
      this.apiCallService.getState(data).subscribe({
        next: (result: any) => {
          if (result) {
            this.StateList = result;
          }
        }, error: (error: any) => {
          this.commonService.showError(error.error);
        }
      })
    } else {
      this.StateList = [];
      this.createEmployee.controls['stateCode'].setValue(null);
    }
  }
  getEmployee() {
    var param = { Cid: "1000", Pid: "1010", GP: "EMP" }
    var data = { param: param }
    this.apiCallService.getBusinessPartnerByCompPlant(data).subscribe({
      next: (result: any) => {
        if (result) {
          this.EmployeeList = result;

          // Assign the data to the data source for the table to render
          this.dataSource = new MatTableDataSource(this.EmployeeList);
          this.dataSource.paginator = this.paginator;
          this.dataSource.sort = this.sort;
        }
      }, error: (error: any) => {
        this.commonService.showError(error.error);
      }
    })
  }
  addEmployee() {
    if (this.createEmployee.invalid) {
      this.submitted = true;
      return;
    }
    var JSON = this.createEmployee.value;
    JSON.createBy = "100"; // Loged IN User ID
    JSON.updateBy = "100"; // Loged IN User ID
    JSON.relationPersons = []
    var data = { data: this.createEmployee.value }
    console.log(data)
    this.apiCallService.addBusinessPartner(data).subscribe({
      next: (result: any) => {
        if (result) {
          this.commonService.showSuccess("Record Inserted Succefully!");
          this.createEmployee.reset();
        }
      }, error: (error: any) => {
        if (error.status == 200) {
          this.commonService.showSuccess(error.error.text);
          this.createEmployee.reset();
        } else {
          this.commonService.showError(error.error);
        }
      }
    })
  }
  editEmployee(row: any) {
    this.updateRow = row;
    this.tabIsActive(1, 1);
    this.getPlant(row.cmpCode);
    this.getState(row.countryCode);
    row.relationPersons = []
    // Use add form for update patch values
    this.createEmployee.patchValue(row)
  }
  deleteMe(row: any) {
    this.deleteRow = row;
    this.openDialogWithTemplateRef(this.deleteDialog);
  }
  deleteEmployee() {
    var head = { cmpCode: this.deleteRow.cmpCode, bpCode: this.deleteRow.bpCode }
    var data = { head: head, param: null }
    this.apiCallService.deleteBusinessPartner(data).subscribe({
      next: (result: any) => {
        if (result) {
          this.commonService.showSuccess("Record Deleted Succefully!");
          const index = this.CompanyList.indexOf(result);
          if (index > -1) { // only splice array when item is found
            this.CompanyList.splice(index, 1); // 2nd parameter means remove one item only
          }
          // Assign the data to the data source for the table to render
          this.dataSource = new MatTableDataSource(this.CompanyList);
        }
      }, error: (error: any) => {
        if (error.status == 200) {
          this.commonService.showSuccess(error.error.text);
        } else {
          this.commonService.showError(error.error);
        }
      }
    })
  }
}

export interface BPEMPData {
  bpCode: string,
  cmpCode: string,
  plantCode: string,
  actGroupCode: string,
  bpFName: string,
  bpMName: string,
  bpLName: string,
  shortKey: string,
  pan: string,
  gstNumber: string,
  addressLine1: string,
  addressLine2: string,
  street: string,
  city: string,
  postalCode: string,
  stateCode: string,
  countryCode: string,
  phone: string,
  email: string,
  status: string,
  createBy: string,
  createOn: string,
  updateBy: string,
  updateOn: string,
  isDeleted: string,
  relationPersons: string,
}


