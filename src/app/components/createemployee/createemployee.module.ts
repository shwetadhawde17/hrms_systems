import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CreateemployeeRoutingModule } from './createemployee-routing.module';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    CreateemployeeRoutingModule
  ]
})
export class CreateemployeeModule { }
