import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {MaterialModule} from '../../app/Shared/material.module';
import { ApiCallService } from '../services/api-call.service';
import { CommonService } from '../services/common.service';
import { NavbarComponent } from './Shared/navbar/navbar.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { FlexLayoutModule } from "@angular/flex-layout";
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { ComponentsComponent } from './components.component';
import { ComponentsRoutingModule } from './components-routing.module';
import { CreatecompanyComponent } from './createcompany/createcompany.component';

import { ProjectcreationComponent } from './projectcreation/projectcreation.component';
import { CreateclientComponent } from './createclient/createclient.component';

import { CreateconsultantComponent } from './createconsultant/createconsultant.component';
import { CreateemployeeComponent } from './createemployee/createemployee.component';
import { ProjectmasterComponent } from './projectmaster/projectmaster.component';
import { DatamasterComponent } from './datamaster/datamaster.component';
import { VendorcreationComponent } from './vendorcreation/vendorcreation.component';
import { StudentDetailsComponent } from './student-details/student-details.component';

import { ProjectdesignationComponent } from './projectdesignation/projectdesignation.component';
import { EmployeedesignationComponent } from './employeedesignation/employeedesignation.component';
import { DepartmentdesignationComponent } from './departmentdesignation/departmentdesignation.component';

// import{ChartsModule} from 'ng2charts';

@NgModule({
  declarations: [
    ComponentsComponent, NavbarComponent, DashboardComponent, CreatecompanyComponent,CreateclientComponent,
    ProjectcreationComponent, CreateconsultantComponent, CreateemployeeComponent, VendorcreationComponent,
    ProjectmasterComponent,DatamasterComponent, StudentDetailsComponent, ProjectdesignationComponent, EmployeedesignationComponent, DepartmentdesignationComponent],
  imports: [
    CommonModule,
    ComponentsRoutingModule,
    MaterialModule,
    FlexLayoutModule,
    FormsModule,
    ReactiveFormsModule,
    // ChartsModule
  ],
  providers:[
    ApiCallService,
    CommonService
  ]
})
export class ComponentsModule { }
