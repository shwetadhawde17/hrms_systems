import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
// import { EmailValidation, mobileValidation } from 'src/app/services/validations';

@Component({
  selector: 'app-employeedesignation',
  templateUrl: './employeedesignation.component.html',
  styleUrls: ['./employeedesignation.component.scss']
})
export class EmployeedesignationComponent implements OnInit {

  empdesForm: FormGroup;

  constructor(private fb: FormBuilder) {
    this.empdesForm = this.fb.group({
      descCode: [null, [Validators.required]],
      descDesc: [null, [Validators.required]],
      cmpCode: [null, [Validators.required]],
      deptCode: [null, [Validators.required]],
      deptDesc: [null, [Validators.required]],
      unitCode: [null, [Validators.required]],
      unitDesc: [null, [Validators.required]],
      actCode: [null, [Validators.required]],
      actDesc: [null, [Validators.required]],
      
    })
  }
  addempdes() {
    console.log("empdesForm", this.empdesForm?.value);
  }

  ngOnInit(): void {
  }

}
