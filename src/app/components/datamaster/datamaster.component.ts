import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { EmailValidation, mobileValidation } from 'src/app/services/validations';

@Component({
  selector: 'app-datamaster',
  templateUrl: './datamaster.component.html',
  styleUrls: ['./datamaster.component.scss']
})
export class DatamasterComponent implements OnInit {
  datamaster: FormGroup

  constructor(private fb: FormBuilder) {
    this.datamaster = this.fb.group({
      masterType: [null, [Validators.required]],
      status: [null, [Validators.required]],
    })
  }
   
  
    // input field hide show
    datamastertype() {
      if (this.datamaster.value.masterType == 'projectDesignation') {
        this.datamaster.removeControl('projID');
        this.datamaster.addControl('projdesc', this.fb.control('', [Validators.required]));
        this.datamaster.addControl('projcliID', this.fb.control('', [Validators.required]));
        this.datamaster.addControl('projcliname',this.fb.control('', [Validators.required]));
        this.datamaster.removeControl('projMngID')
        this.datamaster.addControl('projMngname',this.fb.control('', [Validators.required]));
        this.datamaster.addControl('projStartDate',this.fb.control('', [Validators.required]));
        this.datamaster.addControl('projEndDate',this.fb.control('', [Validators.required]));
        this.datamaster.removeControl('projempId');
        this.datamaster.addControl('projempname',this.fb.control('', [Validators.required]));
        this.datamaster.removeControl('projdeptname');
        // this.datamaster.removeControl('actDesc',this.fb.control('', [Validators.required]))
      }
      else if (this.datamaster.value.masterType == 'departmentDesignation') {
        this.datamaster.addControl('deptid', this.fb.control('', [Validators.required]));
        this.datamaster.addControl('deptname', this.fb.control('', [Validators.required]));
        this.datamaster.addControl('deptcmpname', this.fb.control('', [Validators.required]));
        this.datamaster.addControl('deptmngname', this.fb.control('', [Validators.required]));
        
        // this.datamaster.removeControl('unitDesc')
      }
      else if (this.datamaster.value.masterType == 'empDesignation') {
        this.datamaster.addControl('descCode', this.fb.control('', [Validators.required]));
        this.datamaster.addControl('descDesc', this.fb.control('', [Validators.required]));
        this.datamaster.addControl('cmpCode', this.fb.control('', [Validators.required]));
        this.datamaster.removeControl('deptCode');
        this.datamaster.removeControl('deptDesc');
        this.datamaster.removeControl('unitCode');
        this.datamaster.removeControl('unitDesc');
        this.datamaster.removeControl('actCode');
        this.datamaster.removeControl('actDesc')
      }
     
    }
  
    Adddatamaster() {
      if (this.datamaster.valid) {
        console.log("Success", this.datamaster.value)
      }
      else {
        console.log("fill the form")
      }
  
    }

    ngOnInit(): void {
      
    }
}
