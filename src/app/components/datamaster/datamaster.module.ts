import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DatamasterRoutingModule } from './datamaster-routing.module';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    DatamasterRoutingModule
  ]
})
export class DatamasterModule { }
