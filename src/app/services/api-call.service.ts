import { Injectable } from '@angular/core';
import { data } from 'jquery';
import { CommonService } from './common.service';


@Injectable({
  providedIn: 'root'
})
export class ApiCallService {

  constructor(private commonService: CommonService) { }

  get WindowRef() {
    return window;
  }

  signup = (data: any) => this.commonService.doPost('auth/signup', data);
  getProfileDetails = (data: any) => this.commonService.doGet('api/user', data);
  // Masters
  getCountry = (data: any) => this.commonService.doGet('Country', data);

  getState = (data: any) => this.commonService.doGet('State', data);
  getStateId = (data: any) => this.commonService.doGet('State' + '/' + data.countryCode, data);

  getCompany = (data: any) => this.commonService.doGet('CompanyDetails', data);
  addCompany = (data: any) => this.commonService.doPost('CompanyDetails', data);
  updateCompany = (data: any) => this.commonService.doPut('CompanyDetails' + '/' + data.data.oldCmpCode, data);
  deleteCompany = (data: any) => this.commonService.doDelete('CompanyDetails' + '/' + data.cmpCode, data);

  getPlant = (data: any) => this.commonService.doGet('Plant', data);
  getPlantId = (data: any) => this.commonService.doGet('Plant' + '/' + data.cmpCode, data);
  addPlant = (data: any) => this.commonService.doPost('Plant', data);
  updatePlant = (data: any) => this.commonService.doPut('Plant', data);
  deletePlant = (data: any) => this.commonService.doDelete('Plant' + '/' + data.cmpCode + '/' + data.plantCode, data);

  getBusinessPartner = (data: any) => this.commonService.doGet('BusinessPartner', data);
  getBusinessPartnerByCompPlant = (data: any) => this.commonService.doGet('BusinessPartner' + '/' + 'ByCompPlant', data);
  addBusinessPartner = (data: any) => this.commonService.doPost('BusinessPartner', data);
  updateBusinessPartner = (data: any) => this.commonService.doPut('BusinessPartner' + '/' + data.head.cmpCode + '/' + data.head.bpCode, data);
  deleteBusinessPartner = (data: any) => this.commonService.doDelete('BusinessPartner' + '/' + data.head.cmpCode + '/' + data.head.bpCode, data);

  getProject = (data: any) => this.commonService.doGet('Project', data);
  addProject = (data: any) => this.commonService.doPost('Project', data);
  updateProject = (data: any) => this.commonService.doPut('Project', data);
  deleteProject = (data: any) => this.commonService.doDelete('Project', data);

  getProjectType = (data: any) => this.commonService.doGet('ProjectType', data);
  addProjectType = (data: any) => this.commonService.doPost('ProjectType', data);
  updateProjectType = (data: any) => this.commonService.doPut('ProjectType', data);
  deleteProjectType = (data: any) => this.commonService.doDelete('ProjectType', data);

  executionMaster = (data: any) => this.commonService.doPost('ExecutionType', data);

}