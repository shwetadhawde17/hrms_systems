import { Injectable } from '@angular/core';
import { environment } from './../../environments/environment';
import { ToastrService } from 'ngx-toastr';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable()
export class CommonService {
	dataTable: any;
	private baseUrl = environment.baseUrl;
	headers: any;
	constructor(private router: Router, private toastr: ToastrService, private http: HttpClient) { }


	// Get Method 
	doGet(url: any, qp: any) {			
		this.headers = new HttpHeaders({
			'Content-Type': 'application/json',
			'Access-Control-Allow-Origin': '*',
		});
		console.log("this.headers >>",url, this.headers)
		return this.http.get<any>(this.baseUrl + url, { params: qp.param, headers: this.headers })
	}


	// Post Method 
	doPost(url: any, data: any) {
		this.headers = new HttpHeaders({
			'Content-Type': 'application/json',
			'Access-Control-Allow-Headers': 'Content-Type',
			'Access-Control-Allow-Origin': '*'
		});
		// console.log(this.baseUrl + url, data.data, { params: data.param, headers: this.headers })
		return this.http.post<any>(this.baseUrl + url, data.data, { params: data.param, headers: this.headers, })
	}

	// Post Method for Files(.xlsx, .csv)
	doPostFile(url: any, data: any) {
		this.headers = new HttpHeaders({
			'Access-Control-Allow-Headers': 'Content-Type', 'Access-Control-Allow-Origin': '*',
			'Access-Control-Allow-Credentials': 'true', 'X-CSRF-Token': 'Fetch',
			'Content-Security-Policy': 'upgrade-insecure-requests',
		});
		// console.log(this.baseUrl + url, data.data, { params: data.param, headers: this.headers })
		return this.http.post<any>(this.baseUrl + url, data.data, { params: data.param, headers: this.headers })
	}


	// Put Method
	doPut(url: any, data: any) {
		this.headers = new HttpHeaders({
			'Content-Type': 'application/json',
			'Access-Control-Allow-Origin': '*',
		});
		// console.log(this.baseUrl + url, data.data, { params: data.param, headers: this.headers })
		return this.http.put<any>(this.baseUrl + url, data.data, { params: data.param, headers: this.headers })
	}

	doDelete(url: any, data: any) {
		this.headers = new HttpHeaders({
			'Content-Type': 'application/text',
			'Access-Control-Allow-Origin': '*',
		});
		console.log(this.baseUrl + url,{ params: data.param, headers: this.headers })
		return this.http.delete<any>(this.baseUrl + url,{ params: data.param, headers: this.headers })
	}




	// Success Toastr
	showSuccess(msg: any) {
		this.toastr.success(msg, "Success!", {
			progressBar: true,
			progressAnimation: "decreasing",
			closeButton: true
		})
	}

	// Error Toastr
	showError(msg: any) {
		this.toastr.error(msg, "Error!", {
			progressBar: true,
			progressAnimation: "decreasing",
			closeButton: true
		})
	}




	// Logout User and expired session.
	logout() {
		sessionStorage.removeItem('token');
		this.router.navigate(['login']);
	}

}
