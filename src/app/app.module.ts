
import { NgModule } from '@angular/core';
import { ToastrModule } from 'ngx-toastr'; 
import { AppComponent } from './app.component';
import { CommonModule } from '@angular/common';
import { AppRoutingModule } from './app-routing.module';
import { BrowserModule } from '@angular/platform-browser';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { MainoutletComponent } from './mainoutlet/mainoutlet.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { HttpClientModule,HTTP_INTERCEPTORS } from '@angular/common/http';
import {MaterialModule} from './Shared/material.module';
// import { HttpErrorInterceptor } from './services/http-error.interceptor';
import {MatDialogModule} from '@angular/material/dialog'; 
import {CommonService } from './services/common.service'
import { ApiCallService} from './services/api-call.service'
import {LoginComponent} from './login/login.component';
import { NgxSpinnerModule } from "ngx-spinner";
// import{ChartsModule} from 'ng2charts';
@NgModule({
  declarations: [
    AppComponent,
    MainoutletComponent,
    LoginComponent,
  ],
  imports: [
    BrowserModule,
    MatDialogModule,
    CommonModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    MaterialModule,
    HttpClientModule,
    ToastrModule.forRoot() ,// ToastrModule added
    NgxSpinnerModule,
    // ChartsModule
    
  ],
  providers: [CommonService,ApiCallService],
  bootstrap: [AppComponent]
})
export class AppModule { }
